package com.codehub.eshop;

import com.codehub.eshop.model.Order;
import com.codehub.eshop.model.OrderItem;
import com.codehub.eshop.repository.OrderRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderTest {

    @Autowired
    private OrderRepository orderRepository;

    private static final Logger logger = LoggerFactory.getLogger(EshopApplicationTests.class);

//    @Test
//    public void findAll() {
//        logger.info("ORDERS: {}" + orderRepository.findAll());
//    }

    @Test
    public void findOrderByUserID() {
        logger.info("ORDERS BY USER ID: {}", orderRepository.findOrderByUserID(1L));
    }

    @Test
    public void saveOrderItem() {
        logger.info("ORDERS BY USER ID: {}", orderRepository.findOrderByUserID(1L));
        List<Order>  orders = orderRepository.findOrderByUserID(1L);
        List<OrderItem> orderItems = new ArrayList<>();

        OrderItem orderItem1 = new OrderItem();
        orderItem1.setOrderItemPrice(23L);
        orderItem1.setQuantity(3);

        orderItems.add(orderItem1);

        orders.get(0).setOrderItems(orderItems);

        orderRepository.save(orders.get(0));

        logger.info("ORDERS BY USER ID: {}", orderRepository.findOrderByUserID(1L));
    }

}
