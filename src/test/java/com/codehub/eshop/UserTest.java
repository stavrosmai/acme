package com.codehub.eshop;

import com.codehub.eshop.repository.LoginUserRepository;
import com.codehub.eshop.repository.UsersRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserTest {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    LoginUserRepository loginUserRepository;

    private static final Logger logger = LoggerFactory.getLogger(EshopApplicationTests.class);

    // [User{userID=1, username='marimar', firstname='Maria', lastname='Marinou', address='Damareos 99'}]
    @Test
    public void findAll() {
        logger.info("USERS: {}", usersRepository.findAll());
    }

    @Test
    public void findUsersByUserID() {
        logger.info("USERS BY USERID: {}", usersRepository.findUsersByUserID(2L).toString());
    }

    @Test
    public void doAuthenticate() {
        logger.info("Username : {}", loginUserRepository.findLoginUserByUserId(1L));
    }
}
