package com.codehub.eshop;

import com.codehub.eshop.model.Cart;
import com.codehub.eshop.model.CartItem;
import com.codehub.eshop.model.Product;
import com.codehub.eshop.repository.CartRepository;
import com.codehub.eshop.repository.ProductRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CartTest {

    private static final Logger logger = LoggerFactory.getLogger(EshopApplicationTests.class);

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private ProductRepository productRepository;

    @Test
    public void findCartByUserID() {
        logger.info("CART BY USER: {}", cartRepository.findCartByUserID(1L).toString());
    }

    @Test
    public void addToCart() {
        Optional<Cart> cart = cartRepository.findCartByUserID(1L);
        Product product = productRepository.findProductByProductID(10L);


        logger.info("PRODUCT :  {} ", product);
        logger.info("CART ITEMS LIST SIZE : {} ", cart.get().getCartItems().size());
        if(cart.get().getCartItems().contains(product)){
            logger.info("CONTAINS PRODUCT ! ");
        }

//        cart.getCartItems().get(0).setProducts(product);
//        cart.getCartItems().get(0).setQuantity(6);
//        logger.info("CART : {} ", cart);
//        List<CartItem> cartItems = new ArrayList<>();
//        cartItems.get(0).setProducts(product);


    }

}
