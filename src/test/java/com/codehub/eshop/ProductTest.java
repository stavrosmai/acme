package com.codehub.eshop;

import com.codehub.eshop.repository.ProductCategoryRepository;
import com.codehub.eshop.repository.ProductRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductTest {

    private static final Logger logger = LoggerFactory.getLogger(EshopApplicationTests.class);

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private ProductRepository productRepository;

    @Test
    public void findAll() {
        logger.info("PRODUCT: {}", productCategoryRepository.findByCategoryID(1L));
    }



}
