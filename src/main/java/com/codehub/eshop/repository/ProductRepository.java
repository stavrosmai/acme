package com.codehub.eshop.repository;

import com.codehub.eshop.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sun.plugin.perf.PluginRollup;

import java.util.Collection;
import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {


//    @Query(value = "select p from product p" +
//            "inner join product_category c " +
//            "on p.category_id = c.category_id"
//        ,nativeQuery = true)
//    List<Product> findProductByCategoryID(@Param("categoryId") Long categoryId);

    Product findProductByProductID(Long productId);

    List<Product> findProductsByStockIsNull();

}
