package com.codehub.eshop.repository;

import com.codehub.eshop.model.LoginUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LoginUserRepository extends JpaRepository<LoginUser, String> {

    @Query(value = "delete from login_user where user_id = :userId"
            , nativeQuery = true)
    void deleteLoginUserByUserId(@Param("userId") Long userId);

    Optional<LoginUser> findLoginUserByUserId(Long userId);

}
