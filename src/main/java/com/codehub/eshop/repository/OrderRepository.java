package com.codehub.eshop.repository;

import com.codehub.eshop.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

//    List<Order> findOrderByOrderID(Long orderId);

    List<Order> findOrderByUserID(Long useId);


}
