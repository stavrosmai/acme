package com.codehub.eshop.repository;

import com.codehub.eshop.model.Product;
import com.codehub.eshop.model.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Long> {

    List<ProductCategory> findAll();

    List<ProductCategory> findByCategoryID(Long categoryId);

}

