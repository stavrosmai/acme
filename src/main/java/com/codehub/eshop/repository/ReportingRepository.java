package com.codehub.eshop.repository;

import com.codehub.eshop.dto.BestSeller;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ReportingRepository{


    @Query(name = "Order.findBestSellers", nativeQuery = true)
    Optional<BestSeller> findBestSellers();

}
