package com.codehub.eshop.repository;

import com.codehub.eshop.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersRepository extends JpaRepository<User, Long> {

    List<User> findUsersByUserID(Long userId);

    List<User> findUsersByUsername(String username);

    User findUsersByPassword(String password);

    User findUsersByPasswordAndUsername(String password, String username);

    List<User> findAll();

//    Boolean existsByUsername(String username);

//    @Query("select new com.codehub.eshop.model.User(u.firstname, u.lastname, o.order_id, o.o_date, o.total_price)"
//            + " from users u inner join orders o on u.id = o.userid " +
//            " where u.id = :userId ")
//    List<User> findOrdersByUser(Long userId);

}
