package com.codehub.eshop.config;

import com.codehub.eshop.interceptors.LoggerInterceptor;
import com.codehub.eshop.interceptors.TokenValidationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import java.util.ArrayList;
import java.util.List;

@EnableWebMvc
@Configuration
@ComponentScan("com.codehub.eshop.controllers")
public class WebConfig implements WebMvcConfigurer {

    @Bean
    public TokenValidationInterceptor getTokenValidationInterceptor() { return new TokenValidationInterceptor(); }

    @Bean
    public LoggerInterceptor getLoggerInterceptor() {
        return new LoggerInterceptor();
    }

    @Override
    public void addInterceptors(final InterceptorRegistry registry) {

        List<String> ignoreList = new ArrayList<>();

        registry.addInterceptor(getTokenValidationInterceptor())
                .addPathPatterns("/user/{userId}/cart")
                .addPathPatterns("/user/{userId}/cart/{cartId}")
                .addPathPatterns("/user/{userId}/cart/{cartId}/cancel");


        registry.addInterceptor(getLoggerInterceptor());
    }
}
