package com.codehub.eshop.dto;

import java.io.Serializable;

public class BestSeller implements Serializable {

    private Long productId;

    private Long totalOrders;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getTotalOrders() {
        return totalOrders;
    }

    public void setTotalOrders(Long totalOrders) {
        this.totalOrders = totalOrders;
    }

    public BestSeller(Long productId, Long totalOrders) {
        this.productId = productId;
        this.totalOrders = totalOrders;
    }

    public BestSeller() {
    }
}
