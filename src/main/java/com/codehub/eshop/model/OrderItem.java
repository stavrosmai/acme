package com.codehub.eshop.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="ORDER_ITEM")
public class OrderItem implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "orderItemId", nullable = false)
    private Long orderItemID;

    @Column(name = "quantity", nullable = false)
    private Integer quantity;

    @Column(name="orderItemPrice", nullable = false)
    private Long orderItemPrice;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name ="productId")
    private Product product;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Long getOrderItemID() { return orderItemID; }

    public void setOrderItemID(Long orderItemID) {this.orderItemID = orderItemID; }

    public Integer getQuantity() { return quantity; }

    public void setQuantity(Integer quantity) { this.quantity = quantity; }

    public Long getOrderItemPrice() { return orderItemPrice; }

    public void setOrderItemPrice(Long orderItemPrice) { this.orderItemPrice = orderItemPrice; }



    public OrderItem() {
    }

    public OrderItem(Integer quantity, Long orderItemPrice, Product products) {
        this.quantity = quantity;
        this.orderItemPrice = orderItemPrice;
        this.product = products;
    }

    @Override
    public String toString() {
        return "OrderItem{" +
                "orderItemID=" + orderItemID +
                ", quantity=" + quantity +
                ", orderItemPrice=" + orderItemPrice +
                ", product=" + product +
                '}';
    }
}
