package com.codehub.eshop.model;


import com.codehub.eshop.dto.BestSeller;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;


@NamedNativeQuery(name = "Order.findBestSellers",
        query = "SELECT TOP(10) product_id, SUM(quantity) AS TotalOrders" +
                "FROM order_item" +
                "GROUP BY product_id" +
                "ORDER BY TotalOrders DESC",
        resultSetMapping = "BestSeller")
@SqlResultSetMappings({
        @SqlResultSetMapping(name = "BestSeller",
                classes = @ConstructorResult(
                        targetClass = BestSeller.class,
                        columns = {
                                @ColumnResult(name = "productId", type = Long.class),
                                @ColumnResult(name = "TotalOrders", type = Long.class)
                        }
                )
        )
})

@Entity
@Table(name = "ORDERS")
public class Order implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "orderId", nullable = false)
    private Long orderID;

    @Column(name = "totalPrice", nullable = false)
    private Float totalPrice;

    @Column(name = "oDate")
    private Date date;

    @Column(name = "userid")
    private Long userID;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name ="orderId", nullable = false)
    private List<OrderItem> orderItems;

    public Long getOrderID() {
        return orderID;
    }

    public void setOrderID(Long orderID) {
        this.orderID = orderID;
    }

    public Float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public Order(Float totalPrice, Date date, Long userID, List<OrderItem> orderItems) {
        this.totalPrice = totalPrice;
        this.date = date;
        this.userID = userID;
        this.orderItems = orderItems;
    }

    public Order() {
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderID=" + orderID +
                ", totalPrice=" + totalPrice +
                ", date=" + date +
                ", userID=" + userID +
                ", orderItems=" + orderItems +
                '}';
    }
}

