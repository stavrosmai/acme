package com.codehub.eshop.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "PRODUCT")
public class Product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "productId", nullable = false)
    private Long productID;

    @Column(name = "productName", nullable = false)
    private String productName;

    @Column(name = "descr", nullable = false)
    private String descr;

    @Column(name = "price", nullable = false)
    private Long price;

    @Column(name= "stock", nullable = false)
    private int stock;


    public Long getProductID() {
        return productID;
    }

    public void setProductID(Long productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public Product(String productName, String descr, Long price, int stock) {
        this.productName = productName;
        this.descr = descr;
        this.price = price;
        this.stock = stock;
    }

    public Product() {
    }

    @Override
    public String toString() {
        return "Product{" +
                "productID=" + productID +
                ", productName='" + productName + '\'' +
                ", descr='" + descr + '\'' +
                ", price=" + price +
                ", stock=" + stock +
                '}';
    }
}

