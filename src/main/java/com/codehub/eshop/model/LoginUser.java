package com.codehub.eshop.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "LOGIN_USER")
public class LoginUser implements Serializable {

    @Id
    @Column(name = "userId", nullable = false)
    private Long userId;

    @Column(name = "token")
    private String token;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LoginUser(Long userId, String token) {
        this.userId = userId;
        this.token = token;
    }

    public LoginUser() {
    }
}
