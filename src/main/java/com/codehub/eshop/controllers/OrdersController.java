package com.codehub.eshop.controllers;

import com.codehub.eshop.model.Order;
import com.codehub.eshop.repository.OrderRepository;
import com.codehub.eshop.service.OrderService;
import com.codehub.eshop.service.OrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/user")
public class OrdersController {

    @Autowired
    private OrderService orderService;

    @GetMapping(path = "{userId}/order")
    public ResponseEntity<List> findAll(@PathVariable Long userId) {
        List<Order> orders = orderService.findOrderByUserID(userId);
        return ResponseEntity.status(HttpStatus.OK).body(orders);
    }

}
