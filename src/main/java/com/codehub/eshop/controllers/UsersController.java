package com.codehub.eshop.controllers;

import com.codehub.eshop.dto.AppUser;
import com.codehub.eshop.model.LoginUser;
import com.codehub.eshop.model.User;
import com.codehub.eshop.repository.LoginUserRepository;
import com.codehub.eshop.service.LoginUserService;
import com.codehub.eshop.service.UserService;
import com.codehub.eshop.service.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/user")
public class UsersController {

    @Autowired
    private UserService userService;

    @Autowired
    private LoginUserService loginUserService;

    @Autowired
    private LoginUserRepository loginUserRepository;

    @Autowired
    private Utility utility;

    private static final Logger logger = LoggerFactory.getLogger(UsersController.class);

    @PostMapping(path = "/register")
    public ResponseEntity<?> register(@RequestBody AppUser appUserDTO) {
        try {
            List<User> users = userService.findUsersByUsername(appUserDTO.getUsername());
            if (!users.isEmpty()) {
                logger.info("User with username:" + appUserDTO.getUsername() + " already exists.");
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            } else {
                String encryptedValue = utility.getMD5EncryptedValue(appUserDTO.getPassword());
                logger.info("Encrypted value: {}", encryptedValue);
                User user = new User();
                user.setPassword(encryptedValue);
                user.setUsername(appUserDTO.getUsername());
                user.setAddress(appUserDTO.getAddress());
                user.setFirstname(appUserDTO.getFirstname());
                user.setLastname(appUserDTO.getLastname());
                userService.create(user);
                logger.info("Created user with username: {}", appUserDTO.getUsername());
                return ResponseEntity.status(HttpStatus.CREATED).body(user);
            }

        } catch (Exception e) {
            logger.info("Unable to authenticate user " + appUserDTO.getUsername(), e);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @PostMapping(path = "/login")
    public ResponseEntity<?> login(@RequestHeader String username,
                                   @RequestHeader String password) {
        try {
            String encryptedValue = utility.getMD5EncryptedValue(password);
            User user = userService.findUsersByPasswordAndUsername(encryptedValue, username);
            if (user != null) {
                UUID token = UUID.randomUUID();
                logger.info("Generated token: {}", token);
                LoginUser loginUser = new LoginUser();
                loginUser.setUserId(user.getUserID());
                loginUser.setToken(token.toString());
                loginUserRepository.save(loginUser);
                return new ResponseEntity<>(token, HttpStatus.ACCEPTED);
            } else {
                logger.info("Wrong Credentials.");
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @PostMapping(path = "/{userId}/logout")
    public ResponseEntity<?> logout(@PathVariable Long userId) {
        try {
            loginUserService.deleteLoginUserByUserId(userId);
            return ResponseEntity.status(HttpStatus.OK).body("Logged Out");

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }
}
