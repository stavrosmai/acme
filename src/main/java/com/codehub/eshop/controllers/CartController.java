package com.codehub.eshop.controllers;

import com.codehub.eshop.model.Cart;
import com.codehub.eshop.model.CartItem;
import com.codehub.eshop.model.Order;
import com.codehub.eshop.repository.CartRepository;
import com.codehub.eshop.repository.ProductRepository;
import com.codehub.eshop.service.CartService;
import com.codehub.eshop.service.CartServiceImpl;
import com.codehub.eshop.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class CartController {

    @Autowired
    private CartService cartService;

    @Autowired
    private OrderService orderService;

    @GetMapping(path = "/user/{userId}/cart")
    public ResponseEntity<Cart> show(@PathVariable Long userId) {
        try {
            Optional<Cart> cart = cartService.findCartByUserID(userId);
            return ResponseEntity.status(HttpStatus.OK).body(cart.get());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PostMapping(path = "/user/{userId}/cart")
    public ResponseEntity<?> add(@RequestHeader Long productId,
                                 @RequestHeader Integer quantity,
                                 @PathVariable Long userId) {
        try {
            //List<CartItem> cartItems = cartService.addToCart(userId, productId, quantity);
            return ResponseEntity.status(HttpStatus.CREATED).body(cartService.addToCart(userId, productId, quantity));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @PostMapping(path = "/user/{userId}/cart/{cartId}")
    public ResponseEntity<?> submit(@PathVariable Long userId,
                                         @PathVariable Long cartId){
        try{
            Order order = orderService.createOrder(userId, cartId);
            return new ResponseEntity<>(order, HttpStatus.CREATED);
        } catch(Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @PostMapping(path = "/user/{userId}/cart/{cartId}/cancel")
    public ResponseEntity<?> cancel(@PathVariable Long cartId){
        try{
           cartService.deleteCartByCartID(cartId);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch(Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

}
