package com.codehub.eshop.controllers;

import com.codehub.eshop.dto.BestSeller;
import com.codehub.eshop.service.ReportingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/reports")
public class ReportingController {

    private static final Logger logger = LoggerFactory.getLogger(ReportingController.class);

    @Autowired
    private ReportingService reportingService;

    @GetMapping(path = "/bestsellers")
    public ResponseEntity<?> findBestSellers() {
        try {
            Optional<BestSeller> bestSellers = reportingService.findBestSellers();
            return ResponseEntity.status(HttpStatus.OK).body(bestSellers);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Not Found");
        }
    }

    @GetMapping(path = "/outOfStock")
    public ResponseEntity<?> findOutOfStock() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(reportingService.findOutOfStock());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }
//
//    @GetMapping(path="/submitted")
//    public ResponseEntity<?> findBestSelling(){
//
//    }
//
//    @GetMapping(path="/ordersByUser")
//    public ResponseEntity<?> findBestSelling(){
//
//    }
//
//    @GetMapping(path="/purschasedPerCategory")
//    public ResponseEntity<?> findBestSelling(){
//
//    }
}
