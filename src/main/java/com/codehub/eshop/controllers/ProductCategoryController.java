package com.codehub.eshop.controllers;

import com.codehub.eshop.model.Product;
import com.codehub.eshop.model.ProductCategory;
import com.codehub.eshop.service.ProductCategoryService;
import com.codehub.eshop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductCategoryController {

    @Autowired
    private ProductCategoryService productCategoryService;

    @GetMapping(path = "/")
    public ResponseEntity<List> findAll() {
        List<ProductCategory> productCategories = productCategoryService.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(productCategories);
    }

    @GetMapping(path = "/category/{categoryId}")
    public ResponseEntity<List> findProductByCategoryID(
            @PathVariable Long categoryId) {
        try{
            List<ProductCategory> products = productCategoryService.findByCategoryID(categoryId);
            return ResponseEntity.status(HttpStatus.OK).body(products);
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

    }

}
