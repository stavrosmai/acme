package com.codehub.eshop.controllers;

import com.codehub.eshop.model.Product;
import com.codehub.eshop.model.ProductCategory;
import com.codehub.eshop.service.ProductCategoryService;
import com.codehub.eshop.service.ProductService;
import com.codehub.eshop.service.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;


    @GetMapping(path = "/product")
    public ResponseEntity<List> findAll() {
        List<Product> products = productService.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(products);
    }

//    @GetMapping(path = "/product/category")
//    public ResponseEntity<List> findProductByCategoryID(
//            @RequestHeader Long categoryId) {
//        List<Product> products = productCategoryService.findByCategoryID(categoryId);
//        return ResponseEntity.status(HttpStatus.OK).body(products);
//    }
}
