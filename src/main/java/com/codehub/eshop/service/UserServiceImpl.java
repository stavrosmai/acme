package com.codehub.eshop.service;

import com.codehub.eshop.model.User;
import com.codehub.eshop.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public List<User> findUsersByUserID(Long userId) {
        List<User> users = usersRepository.findUsersByUserID(userId);
        return users;
    }

    @Override
    public List<User> findUsersByUsername(String username) {
        List<User> users = usersRepository.findUsersByUsername(username);
        return users;
    }

    @Override
    public User findUsersByPasswordAndUsername(String password, String username) {
        User user = usersRepository.findUsersByPasswordAndUsername(password, username);
        return user;
    }

    @Override
    public User create(User entity) {
        return usersRepository.save(entity);
    }

    @Override
    public void update(User entity) {
        usersRepository.save(entity);
    }

    @Override
    public void delete(User entity) {
        usersRepository.delete(entity);
    }

    @Override
    public Optional<User> findById(Long id) {
        return usersRepository.findById(id);
    }

    @Override
    public List<User> findAll() {
        return usersRepository.findAll();
    }
}
