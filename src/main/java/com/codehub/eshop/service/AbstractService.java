package com.codehub.eshop.service;

import java.util.List;
import java.util.Optional;

public interface AbstractService<T, ID> {
    T create(final T entity);

    void update(final T entity);

    void delete(final T entity);

    Optional<T> findById(ID id);

    List<T> findAll();
}
