package com.codehub.eshop.service;

import com.codehub.eshop.model.LoginUser;
import com.codehub.eshop.repository.LoginUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LoginUserServiceImpl implements LoginUserService {

    private static final Logger logger = LoggerFactory.getLogger(LoginUserServiceImpl.class);

    @Autowired
    private LoginUserRepository loginUserRepository;

    @Override
    public void deleteLoginUserByUserId(Long userId) {
        loginUserRepository.deleteLoginUserByUserId(userId);
    }

    @Override
    public boolean doAuthenticate(Long userId, String token) {
        logger.info("Try to validate with userId: " + userId + " and token: " + token);
        Optional<LoginUser> loginUser = loginUserRepository.findLoginUserByUserId(userId);
        if (loginUser.isPresent()) {
            return loginUser.get().getToken().equals(token);
        } else {
            return false;
        }
//        return true;
    }

    @Override
    public List<LoginUser> findAll() {
        return null;
    }

}
