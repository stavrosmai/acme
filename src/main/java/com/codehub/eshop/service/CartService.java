package com.codehub.eshop.service;

import com.codehub.eshop.model.Cart;
import com.codehub.eshop.model.CartItem;
import com.codehub.eshop.model.Order;
import com.codehub.eshop.model.Product;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface CartService extends AbstractService<Cart, Long> {

    Cart addToCart(Long userId, Long productId, Integer quantity);

    void initCart(Cart cart, Product product, int quantity);

    void updateCart(Cart cart, Product product, int quantity);

    Optional<Cart> findCartByUserID(Long userId);

    void deleteCartByCartID(Long id);

}


