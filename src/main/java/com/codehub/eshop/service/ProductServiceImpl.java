package com.codehub.eshop.service;

import com.codehub.eshop.model.Product;
import com.codehub.eshop.model.ProductCategory;
import com.codehub.eshop.repository.ProductCategoryRepository;
import com.codehub.eshop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService{

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private ProductRepository productRepository;

//    @Override
//    public List<Product> findProductByCategoryID(Long categoryId){
//        return productRepository.findProductByCategoryID(categoryId);
//    }

    @Override
    public Product create(Product entity) {
        return productRepository.save(entity);
    }

    @Override
    public void update(Product entity) {
        productRepository.save(entity);
    }

    @Override
    public void delete(Product entity) {
        productRepository.delete(entity);
    }

    @Override
    public Optional<Product> findById(Long id) {
        return productRepository.findById(id);
    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }
}
