package com.codehub.eshop.service;

import com.codehub.eshop.model.Product;

import java.util.List;
import java.util.Optional;

public interface ReportingService {

    Optional findBestSellers();

    List<Product> findOutOfStock();
}
