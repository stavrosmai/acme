package com.codehub.eshop.service;

import com.codehub.eshop.model.User;

import java.util.List;

public interface UserService extends AbstractService<User, Long> {

    List<User> findUsersByUserID(Long userId);

    List<User> findUsersByUsername(String username);

    User findUsersByPasswordAndUsername(String password, String username);
}
