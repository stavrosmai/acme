package com.codehub.eshop.service;

import com.codehub.eshop.model.Product;
import com.codehub.eshop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReportingServiceImpl implements  ReportingService{

    @Autowired
    private ReportingService reportingService;

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Optional findBestSellers() {
        return reportingService.findBestSellers();
    }

    @Override
    public List<Product> findOutOfStock() {
        return productRepository.findProductsByStockIsNull();
    }
}
