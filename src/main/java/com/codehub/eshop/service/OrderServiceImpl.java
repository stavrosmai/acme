package com.codehub.eshop.service;

import com.codehub.eshop.model.Cart;
import com.codehub.eshop.model.Order;
import com.codehub.eshop.model.OrderItem;
import com.codehub.eshop.repository.OrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.sql.Date;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;


    @Autowired
    private CartService cartService;

    private static final Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);


    @Override
    public List<Order> findOrderByUserID(Long orderId) {
        List<Order> orders = orderRepository.findOrderByUserID(orderId);
        return orders;
    }

    @Override
    public Order createOrder(Long userId, Long cartId) {
        Optional<Cart> cart = cartService.findCartByUserID(userId);
        logger.info("Cart id: {}", cart.get().getCartID());
        Order order = new Order();
        List<OrderItem> orderItems = new ArrayList<>();
        Float cost = 0F;
        for (int i = 0; i < cart.get().getCartItems().size(); i++) {
            logger.info("i : {}", i);

            OrderItem orderItem = new OrderItem();
            orderItem.setProduct(cart.get().getCartItems().get(i).getProducts());
            logger.info("Product: {}", orderItem.getProduct());

            orderItem.setQuantity(cart.get().getCartItems().get(i).getQuantity());
            logger.info("Quantity: {}", orderItem.getQuantity());

            orderItem.setOrderItemPrice(cart.get().getCartItems().get(i).getProducts().getPrice());
            orderItem.setOrderItemID(cart.get().getCartItems().get(i).getCartItemID());
            logger.info("Order item: {}", orderItem);

            orderItems.add(orderItem);
            logger.info("Order items list: {}", orderItems);

            cost += Float.valueOf(cart.get().getCartItems().get(i).getProducts().getPrice()) * cart.get().getCartItems().get(i).getQuantity();
            order.setUserID(userId);
            logger.info("Order user id: {}", order.getUserID());

        }
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            //Date now = new Date();
            order.setDate(formatter.parse("29/11/2018"));
        } catch (Exception e) {
            logger.info("DATE ERROR: {}", e.getMessage());
        }
        order.setOrderItems(orderItems);
        order.setOrderID(cart.get().getCartID());
        order.setTotalPrice(cost);
        logger.info("Order item list: {}", order.getOrderItems());
        logger.info("Order: {}", order);
        return create(order);
    }

    @Override
    public Order create(Order entity) {
        return orderRepository.save(entity);
    }

    @Override
    public void update(Order entity) {
        orderRepository.save(entity);
    }

    @Override
    public void delete(Order entity) {
        orderRepository.delete(entity);
    }

    @Override
    public Optional<Order> findById(Long id) {
        return orderRepository.findById(id);
    }

    @Override
    public List<Order> findAll() {
        return orderRepository.findAll();
    }
}
