package com.codehub.eshop.service;

import com.codehub.eshop.model.Product;
import com.codehub.eshop.model.ProductCategory;
import com.codehub.eshop.repository.ProductCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductCategoryServiceImpl implements ProductCategoryService{

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Override
    public  List<ProductCategory> findByCategoryID(Long categoryId){
        return productCategoryRepository.findByCategoryID(categoryId);
    }
    @Override
    public ProductCategory create(ProductCategory entity) {
        return productCategoryRepository.save(entity);
    }

    @Override
    public void update(ProductCategory entity) {
        productCategoryRepository.save(entity);
    }

    @Override
    public void delete(ProductCategory entity) {
        productCategoryRepository.delete(entity);
    }

    @Override
    public Optional<ProductCategory> findById(Long id) {
        return productCategoryRepository.findById(id);
    }

    @Override
    public List<ProductCategory> findAll() {
        return productCategoryRepository.findAll();
    }
}
