package com.codehub.eshop.service;

import com.codehub.eshop.model.Product;
import com.codehub.eshop.model.ProductCategory;

import java.util.List;

public interface ProductCategoryService extends AbstractService<ProductCategory, Long>{

    List<ProductCategory> findByCategoryID(Long categoryId);
}
