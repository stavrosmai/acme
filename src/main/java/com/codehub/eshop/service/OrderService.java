package com.codehub.eshop.service;

import com.codehub.eshop.model.Order;

import java.util.List;

public interface OrderService extends AbstractService<Order, Long> {

    List<Order> findOrderByUserID(Long orderId);

    Order createOrder(Long userId, Long cartId);
}
