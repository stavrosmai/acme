package com.codehub.eshop.service;

import com.codehub.eshop.model.LoginUser;
import java.util.List;

public interface LoginUserService<T, ID> {

    void deleteLoginUserByUserId(Long userId);

    boolean doAuthenticate(Long userId, String token);

    List<T> findAll();

//    T create(final T entity);
}
