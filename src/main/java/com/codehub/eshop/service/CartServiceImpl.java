package com.codehub.eshop.service;

import com.codehub.eshop.model.*;
import com.codehub.eshop.repository.CartRepository;
import com.codehub.eshop.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private ProductService productService;

    @Autowired
    private OrderService orderService;

    private static final Logger logger = LoggerFactory.getLogger(CartService.class);

    @Override
    public Optional<Cart> findCartByUserID(Long userId){
        return cartRepository.findCartByUserID(userId);
    }

    @Override
    public Cart addToCart(Long userId, Long productId, Integer quantity) {

        Optional<Cart> cart = cartRepository.findCartByUserID(userId);
        Optional<Product> product = productService.findById(productId);

        if (cart.get().getCartItems().isEmpty()) {
            initCart(cart.get(), product.get(), quantity);
        } else {
            updateCart(cart.get(), product.get(), quantity);
        }
        cartRepository.save(cart.get());
        return cart.get();


    }

    @Override
    public void initCart(Cart cart, Product product, int quantity) {

        CartItem cartItem = new CartItem();

        if (product.getStock() >= quantity) {
            cartItem.setQuantity(quantity);
            cartItem.setProducts(product);
            cartItem.getProducts().setStock(product.getStock() - quantity);
            cart.getCartItems().add(cartItem);
            logger.info("Product with id : {} added to cart", product.getProductID());
        } else {
            logger.info("Product with id : {} out of stock", product.getProductID());
            logger.info("Available product quantity : {} ", product.getStock());
        }

    }

    @Override
    public void updateCart(Cart cart, Product product, int quantity) {

        CartItem cartItem = new CartItem();

        for (int i = 0; i < cart.getCartItems().size(); i++) {
            Long listProductId = cart.getCartItems().get(i).getProducts().getProductID();

            if (!listProductId.equals(product.getProductID())) {
                cartItem.setQuantity(quantity);
                cartItem.setProducts(product);

            } else {
                cartItem = cart.getCartItems().get(i);
                int newQuantity = quantity + cartItem.getQuantity();
                cartItem.setQuantity(newQuantity);
            }
        }
        if (cartItem.getProducts().getStock() >= quantity) {
            cartItem.getProducts().setStock(cartItem.getProducts().getStock() - quantity);
            cart.getCartItems().add(cartItem);
            logger.info("Product with id : {} added to cart", product.getProductID());
        } else {
            logger.info("Product with id : {} out of stock", product.getProductID());
            logger.info("Available product quantity : {} ", product.getStock());
        }
    }
    @Override
    public void deleteCartByCartID(Long id){
        cartRepository.deleteCartByCartID(id);
    }

    @Override
    public Cart create(Cart entity) {
        return cartRepository.save(entity);
    }

    @Override
    public void update(Cart entity) {
        cartRepository.save(entity);
    }

    @Override
    public void delete(Cart entity) {
        cartRepository.delete(entity);
    }

    @Override
    public Optional<Cart> findById(Long id) {
        return cartRepository.findById(id);
    }

    @Override
    public List<Cart> findAll() {
        return cartRepository.findAll();
    }
}
